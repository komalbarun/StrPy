## StrPy
--------
 Basic pythonic string functions not available in php.

## Installation
----------------
 composer require komalbarun\strpy

## Dependencies
----------------
 * komalbarun\StrPy
 * php >= 7.0

## Usage
---------
```php
<?php

require_once './vendor/autoload.php';

use Komalbarun\StrPy;

$string = new StrPy('Hello')

// By default not case sensitive.
// This returns true.
echo $string->startswith('h');

// Case sensitive.
// Returns false.
echo $string->startswith('h', true);

// Same with endswith()
// Not case sensitive;
// Returns true
echo $string->endswith('O');

// Case sensitive
// Returns Flase.
echo  $string->endswith('O', true);

// returns case inverted string.
// hELLO
echo $string->swapcase();
```
