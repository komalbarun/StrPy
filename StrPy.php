<?php 

declare(strict_types=1);

namespace Komalbarun;

class StrPy
{	
	public function __construct(string $str)
	{
		$this->word = $str;
	}
	
	/**
	 * Returns true if first character in $this->word is $first.
	 * 
	 * @param  string $first first character to match
	 * @param bool $case_sens enable case sensitivity 
	 * @return boolean        
	 */
	public function startswith(string $first, bool $case_sens=false): bool
	{
		if($case_sens)
		{
			return str_split($this->word)[0] === $first;
		}
		
		$first_letter = str_split($this->word)[0];

		if(!empty($first_letter))
		{
			if(stristr($first, $first_letter))
			{
				return true;
			}
		}
		
		return false;
	}

    /**
     * Returns true if last character in $this->word is $end.
     * 
     * @param  string $end first character to match
     * @param bool $case_sens enable case sensitivity 
     * @return boolean  
     */
	public function endswith(string $end, bool $case_sens=false): bool
	{	
		$letters = str_split($this->word);

		if($case_sens)
		{
			return end($letters) === $end;
		}
		
		$last_letter = end($letters);

		if(!empty($last_letter))
		{
			if(stristr($end, $last_letter))
			{
				return true;
			}
		}
		
		return false;
	}

    /**
     * Loops over each character in string $this->word.
     * 
     * @return string 
     */
	public function loop_str()
	{
		$count = strlen($this->word);
		
		for($i=0; $i < $count; $i++)
		{
			yield $this->word[$i];
		}
	}

    /**
     * Converts lowercase characters to uppercase and vice-versa.
     * 
     * @return string 
     */
	public function swapcase(): string
	{	
		$result = '';

		foreach($this->loop_str() as $char)
		{
			if(ctype_lower($char))
			{
				$uchar = strtoupper($char);
				$result .= $uchar;
			}
			else
			{
				$char = strtolower($char);
				$result .= $char;
			}
			
		}

		return $result;
	}
}
